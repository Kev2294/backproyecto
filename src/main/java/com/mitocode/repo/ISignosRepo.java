package com.mitocode.repo;

import com.mitocode.model.Paciente;
import com.mitocode.model.SignosVitales;

//@Repository
public interface ISignosRepo extends IGenericRepo<SignosVitales, Integer>{

}