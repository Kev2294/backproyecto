package com.mitocode.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.IOException;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Archivo;
import com.mitocode.model.SignosVitales;
import com.mitocode.model.SignosVitales;
import com.mitocode.service.IArchivoService;

import com.mitocode.service.ISignosService;

@RestController
@RequestMapping("/signos")
public class SignosController {

	@Autowired
	private ISignosService service;
	
	

	@GetMapping
	public ResponseEntity<List<SignosVitales>> listar() throws Exception {
		List<SignosVitales> lista = service.listar();
		return new ResponseEntity<List<SignosVitales>>(lista, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<SignosVitales> listarPorId(@PathVariable("id") Integer id) throws Exception {
		SignosVitales obj = service.listarPorId(id);

		if (obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<SignosVitales>(obj, HttpStatus.OK);
	}

	

	/*
	 * @PostMapping public ResponseEntity<SignosVitales> registrar(@Valid @RequestBody
	 * SignosVitales p) { SignosVitales obj = service.registrar(p); return new
	 * ResponseEntity<SignosVitales>(obj, HttpStatus.CREATED); }
	 */

	@PostMapping
	public ResponseEntity<SignosVitales> registrar(@Valid @RequestBody SignosVitales p) throws Exception {
		SignosVitales obj = service.registrar(p);

		// localhost:8080/SignosVitaless/2
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(obj.getIdSignos()).toUri();
		return ResponseEntity.created(location).build();
	}

	@PutMapping
	public ResponseEntity<SignosVitales> modificar(@Valid @RequestBody SignosVitales p) throws Exception {
		SignosVitales obj = service.modificar(p);
		return new ResponseEntity<SignosVitales>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		SignosVitales obj = service.listarPorId(id);

		if (obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}

		service.eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<SignosVitales>> listarPageable(Pageable pageable) throws Exception{
		Page<SignosVitales> SignosVitaless = service.listarPageable(pageable);
		return new ResponseEntity<Page<SignosVitales>>(SignosVitaless, HttpStatus.OK);
	}
	
		
}
