package com.mitocode.service.impl;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitocode.dto.ConsultaListaExamenDTO;
import com.mitocode.dto.ConsultaResumenDTO;
import com.mitocode.dto.FiltroConsultaDTO;
import com.mitocode.model.Consulta;
import com.mitocode.model.Paciente;
import com.mitocode.model.SignosVitales;
import com.mitocode.repo.IConsultaExamenRepo;
import com.mitocode.repo.IConsultaRepo;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.repo.IPacienteRepo;
import com.mitocode.repo.ISignosRepo;
import com.mitocode.service.IConsultaService;
import com.mitocode.service.ISignosService;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class SignosServiceImpl extends CRUDImpl<SignosVitales, Integer> implements ISignosService {

	@Autowired
	private ISignosRepo repo;

	@Override
	protected IGenericRepo<SignosVitales, Integer> getRepo() {
		return repo;
	}
	
	@Override
	public Page<SignosVitales> listarPageable(Pageable pageable) {
		return repo.findAll(pageable);
}
}
