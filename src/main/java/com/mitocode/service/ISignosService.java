package com.mitocode.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.dto.ConsultaListaExamenDTO;
import com.mitocode.dto.ConsultaResumenDTO;
import com.mitocode.dto.FiltroConsultaDTO;
import com.mitocode.model.Consulta;
import com.mitocode.model.Paciente;
import com.mitocode.model.SignosVitales;

public interface ISignosService extends ICRUD<SignosVitales, Integer>{
	
	Page<SignosVitales> listarPageable(Pageable pageable);
}
