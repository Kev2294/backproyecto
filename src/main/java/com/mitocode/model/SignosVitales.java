package com.mitocode.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.ISBN;

@Entity
@Table(name = "signo") // , schema = "m01")
public class SignosVitales {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idSignos;

	// JPQL | FROM Consulta c WHERE c.paciente.nombres = '';
	// private Integer idPaciente;

	@ManyToOne
	@JoinColumn(name = "id_paciente", nullable = false, foreignKey = @ForeignKey(name = "FK_consulta_paciente"))
	private Paciente id_paciente;


	@Column(name = "fecha", nullable = false)
	private LocalDateTime fecha;
	
	@Column(name = "temperatura", length = 20, nullable = true)
	private String temperatura;
	
	@Column(name = "pulso", length = 20, nullable = true)
	private String pulso;
	
	@Column(name = "ritmo", length = 20, nullable = true)
	private String ritmo;

	public Integer getIdSignos() {
		return idSignos;
	}

	
	public Paciente getId_paciente() {
		return id_paciente;
	}


	public void setId_paciente(Paciente id_paciente) {
		this.id_paciente = id_paciente;
	}


	public void setIdSignos(Integer idSignos) {
		this.idSignos = idSignos;
	}

	
	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}

	public String getPulso() {
		return pulso;
	}

	public void setPulso(String pulso) {
		this.pulso = pulso;
	}

	public String getRitmo() {
		return ritmo;
	}

	public void setRitmo_respiratorio(String ritmo) {
		this.ritmo = ritmo;
	}
	

	
}
